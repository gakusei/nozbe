# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Title, Name


admin.site.register(Title)
admin.site.register(Name)