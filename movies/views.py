# -*- coding: utf-8 -*-
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter

from .models import Title, Name
from .serializers import TitleSerializer, NameSerializer


class TitleViewSet(viewsets.ModelViewSet):
    queryset = Title.objects.all().order_by('primaryTitle')
    serializer_class = TitleSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('startYear',)

    def get_queryset(self):
        queryset = Title.objects.all()
        genre = self.request.query_params.get('genre', None)
        if genre is not None:
            queryset = queryset.filter(genres__icontains=genre)
        return queryset

class NameViewSet(viewsets.ModelViewSet):
    queryset = Name.objects.all()
    serializer_class = NameSerializer
    filter_backends = (SearchFilter,)
    search_fields = ('primaryName',)
