import urllib2
import gzip
import shutil
import os
from movies.models import Name, Title
import csv

# url for files
urls = ["https://www.dropbox.com/s/3do9bu0awq048uh/title.basics.tsv.gz?dl=1",
        "https://www.dropbox.com/s/xaidig3yw2viyym/name.basics.tsv.gz?dl=1"]

#download files and unpack
for url in urls:
    file = urllib2.urlopen(url)
    file_name = url.split("/")[-1][:-5]
    with open(file_name,'wb') as output:
        output.write(file.read())
        output.close()

    with gzip.open(file_name, 'rb') as f_in, open(file_name[:-3], 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)

    if os.path.isfile(file_name):
        os.remove(file_name)
    else:  ## Show an error ##
        print("Error: %s file not found" % file_name)

# create objects to populate tables
with open("title.basics.tsv") as tsvfile:
    tsvfile.readline()
    for line in csv.reader(tsvfile, dialect="excel-tab"):
        if line[7] != '\N':
            runtime = int(line[7])
        else:
            runtime = None

        genres_temp =  line[8].split(",")

        p, created = Title.objects.get_or_create(tconst = line[0], titleType = line[1], primaryTitle = line[2],
                                                 originalTitle = line[3], isAdult = line[4], startYear = line[5],
                                                 endYear = line[6], runtimeMinutes = runtime, genres = genres_temp)

with open("name.basics.tsv") as tsvfile:
    tsvfile.readline()
    for line in csv.reader(tsvfile, dialect="excel-tab"):
        primaryProfession_temp = line[4].split(",")

        p, created = Name.objects.get_or_create(nconst = line[0], primaryName = line[1], birthYear = line[2], deathYear = line[3],
                                                primaryProfession = primaryProfession_temp)
        knowForTitles_temp = line[5].split(",")
        name = Name.objects.get(nconst=line[0])
        for t in knowForTitles_temp:
            try:
                title = Title.objects.get(tconst = t)
                name.knownForTitles.add(title)
            except:
                print "Missing title"

# remove files
for url in urls:
    file_name = url.split("/")[-1][:-8]
    if os.path.isfile(file_name):
        os.remove(file_name)
    else:  ## Show an error ##
        print("Error: %s file not found" % file_name)
