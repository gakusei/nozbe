from rest_framework.routers import SimpleRouter
from movies.views import TitleViewSet, NameViewSet

router = SimpleRouter()
router.register(r'title', TitleViewSet)
router.register(r'name', NameViewSet)

urlpatterns = router.urls


### ENDPOITNS ###
#[domain]/title/                            :list of all titles
#[domain]/name/                             :list of all names
#[domain]/title/?startYear=[year_number]    :get titles from selected year
#[domain]/title/?genre=[genre]              :get titles with selected one genre
#[domain]/name/?search=[name or surname]    :get actor via search his name and show titles of his movies
