# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import json


class Title(models.Model):
    tconst = models.CharField(max_length=255, unique=True, null=False, blank=False)
    titleType = models.CharField(max_length=255)
    primaryTitle = models.CharField(max_length=255)
    originalTitle = models.CharField(max_length=255, null=True)
    isAdult = models.BooleanField()
    startYear = models.CharField(max_length=255)
    endYear = models.CharField(max_length=255)
    runtimeMinutes = models.IntegerField(null=True)
    genres = models.CharField(max_length=255)

    def __unicode__(self):
        return self.tconst

    def setgenres(self, x):
        self.foo = json.dumps(x)

    def getgenres(self):
        return json.loads(self.foo)


class Name(models.Model):
    nconst = models.CharField(max_length=255, unique=True, null=False, blank=False)
    primaryName = models.CharField(max_length=255)
    birthYear = models.CharField(max_length=255)
    deathYear = models.CharField(max_length=255)
    primaryProfession = models.CharField(max_length=255)
    knownForTitles = models.ManyToManyField(Title, blank=False)

    def __unicode__(self):
        return self.nconst

    def setprimaryProfession(self, x):
        self.primaryProfession = json.dumps(x)

    def getprimaryProfession(self):
        return json.loads(self.primaryProfession)
