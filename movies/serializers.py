from .models import Title, Name
from rest_framework import serializers


class TitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Title
        fields = '__all__'

class TitleNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Title
        fields = ('primaryTitle',)

class NameSerializer(serializers.ModelSerializer):
    movieTitles = TitleNameSerializer(source='knownForTitles', many=True)

    class Meta:
        model = Name
        fields = ('nconst', 'primaryName', 'birthYear', 'deathYear', 'primaryProfession', 'movieTitles')
