# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from movies.models import Title, Name


class TitleTests(APITestCase):
    def setUp(self):
        self.title = Title.objects.create(tconst = 'testTconts', titleType = 'test', primaryTitle = 'test',
                                          originalTitle = 'test', isAdult = True, startYear = '1000',
                                          endYear = '1000', runtimeMinutes = 100)

    def test_get_title_list(self):
        url = reverse('title-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_title(self):
        response = self.client.get('/title/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class NameTests(APITestCase):
    def setUp(self):
        self.name = Name.objects.create(nconst = 'testNconts', primaryName = 'test', birthYear = 'test',
                                        deathYear = 'test',)

    def test_get_name_list(self):
        url = reverse('name-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_name(self):
        response = self.client.get('/name/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
