# nozbe imdb

Serwis IMDB udostępnia część swojej bazy filmów w postaci plików TSV
http://www.imdb.com/interfaces/
Przygotuj w Pythonie skrypt, który zaimportuje zawartość title.basics.tsv.gz oraz name.basics.tsv.gz do bazy PostgreSQL. Dane powinny trafić do nie więcej niż 3 tabel. Zatem nie przewidujemy dla pól takich jak genres, primaryProfession  osobnych tabel, ale jednocześnie - inaczej niż jest to napisane w ich dokumentacji - zakładamy, że w przyszłości mogą one zawierać więcej niż te 3 elementy w tablicy (np. 5 lub 7), więc trzeba mieć to w tyle głowy.
Oprócz importera, stwórz w Pythonie zalążek REST API, które teoretycznie można by w przyszłości dalej rozwijać. Podstawowy wymóg to endpointy, które pozwolą:
Pobrać listę - wg kolejności alfabetycznej - wszystkich tytułów filmów o wskazanej wartości startYear, wraz z powiązanymi z nimi osobami (mechanizm z paginacją wyników).
Jak wyżej, ale z możliwością wylistowania filmów z wskazanym genre.
Zwrócić filmy, z którymi związane są osoby pasujące do wyników wyszukiwania (czyli jako parametr przekazujemy frazę - np. z nazwiskiem, na podstawie której szukamy ludzi i dla każdego z nich zwracamy listę tytułów filmów).

## Getting Started
create database on postresql

	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'database name',
		'USER': 'username',
		'PASSWORD': 'password',
		'HOST': 'localhost',
		'PORT': '',
	    }


connect it in settings
install requirements.txt

	pip install requirements.txt

migrate database

	manage.py makemigrations
	manage.py migrate

run script to populate database (take some time for ~ 7mln records for tabe, I should crate progress bar)

	python manage.py import_imdb_data


## Endpoints

	[domain]/title/                            :list of all titles
	[domain]/name/                             :list of all names

	[domain]/title/?startYear=[year_number]    :get titles from selected year
	[domain]/title/?genre=[genre]              :get titles with selected one genre
	[domain]/name/?search=[name or surname]    :get actor via search his name and show titles of his movies

